# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .handlers import (
    CaseAssigneeEmailEnqueuedHandler,
    CaseAssigneeSetHandler,
    CaseAttributesChangedHandler,
    CaseCreatedHandler,
    CaseV2AttributeChangedHandler,
    DocumentsEnqueuedHandler,
    EmailsEnqueuedHandler,
    ObjectActionTriggeredRequestedHandler,
    SubcasesEnqueuedHandler,
    SubjectRelationCreatedHandler,
    SubjectRelationEmailEnqueuedHandler,
    SubjectsEnqueuedHandler,
    TimelineExportRequestedHandler,
)
from minty_amqp.consumer import BaseConsumer


class CaseEventsConsumer(BaseConsumer):
    def _register_routing(self):
        self._known_handlers = [
            CaseAssigneeEmailEnqueuedHandler(self.cqrs),
            CaseAssigneeSetHandler(self.cqrs),
            CaseAttributesChangedHandler(self.cqrs),
            CaseCreatedHandler(self.cqrs),
            DocumentsEnqueuedHandler(self.cqrs),
            EmailsEnqueuedHandler(self.cqrs),
            SubcasesEnqueuedHandler(self.cqrs),
            SubjectRelationCreatedHandler(self.cqrs),
            SubjectRelationEmailEnqueuedHandler(self.cqrs),
            SubjectsEnqueuedHandler(self.cqrs),
            TimelineExportRequestedHandler(self.cqrs),
            ObjectActionTriggeredRequestedHandler(self.cqrs),
            CaseV2AttributeChangedHandler(self.cqrs),
        ]

        self.routing_keys = []
        for handler in self._known_handlers:
            self.routing_keys.extend(handler.routing_keys)
