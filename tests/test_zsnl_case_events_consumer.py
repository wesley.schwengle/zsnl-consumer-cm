# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from minty.exceptions import CQRSException
from unittest import TestCase, mock
from uuid import uuid4
from zsnl_case_events_consumer import __main__
from zsnl_case_events_consumer.constants import (
    case_legacy_prefix,
    case_prefix,
    subject_relation_prefix,
    timeline_export_prefix,
)
from zsnl_case_events_consumer.consumers import CaseEventsConsumer
from zsnl_case_events_consumer.handlers import (
    CaseAssigneeEmailEnqueuedHandler,
    CaseAssigneeSetHandler,
    CaseAttributesChangedHandler,
    CaseCreatedHandler,
    CaseV2AttributeChangedHandler,
    DocumentsEnqueuedHandler,
    EmailsEnqueuedHandler,
    ObjectActionTriggeredRequestedHandler,
    SubcasesEnqueuedHandler,
    SubjectRelationCreatedHandler,
    SubjectRelationEmailEnqueuedHandler,
    SubjectsEnqueuedHandler,
    TimelineExportRequestedHandler,
)


class TestCaseEventsConsumers(TestCase):
    def test_enqueued_handler(self):
        mock_cqrs = mock.MagicMock()
        mock_command_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = mock_command_instance
        mock_event = mock.MagicMock()
        mock_event.correlation_id = "fake_correlaton_id"
        mock_event.context = "fake_context"
        mock_event.user_uuid = "fake_user_uuid"
        mock_event.entity_id = "fake_uuid"
        prefix = "zsnl.v2.zsnl_domains_case_management.Case."

        test_cases = [
            {
                "routing_keys": [prefix + "SubcasesEnqueued"],
                "event_name": "SubcasesEnqueued",
                "changes_key": "enqueued_subcases_data",
                "handler": SubcasesEnqueuedHandler,
                "actual_call": mock_command_instance.create_subcases,
            },
            {
                "routing_keys": [prefix + "DocumentsEnqueued"],
                "event_name": "DocumentsEnqueued",
                "changes_key": "enqueued_documents_data",
                "handler": DocumentsEnqueuedHandler,
                "actual_call": mock_command_instance.generate_documents,
            },
            {
                "routing_keys": [prefix + "EmailsEnqueued"],
                "event_name": "EmailsEnqueued",
                "changes_key": "enqueued_emails_data",
                "handler": EmailsEnqueuedHandler,
                "actual_call": mock_command_instance.generate_emails,
            },
            {
                "routing_keys": [prefix + "SubjectsEnqueued"],
                "event_name": "SubjectsEnqueued",
                "changes_key": "enqueued_subjects_data",
                "handler": SubjectsEnqueuedHandler,
                "actual_call": mock_command_instance.generate_subjects,
            },
        ]

        for test_case in test_cases:
            handler = test_case["handler"](mock_cqrs)
            assert handler.routing_keys == test_case["routing_keys"]
            assert handler.cqrs == mock_cqrs

            mock_event.event_name = test_case["event_name"]
            mock_event.changes = [
                {
                    "key": test_case["changes_key"],
                    "old_value": None,
                    "new_value": "{}",
                }
            ]

            handler.handle(mock_event)
            test_case["actual_call"].assert_called_once_with(
                case_uuid="fake_uuid", queue_ids=[]
            )

    def test_case_created_handler(self):
        mock_cqrs = mock.MagicMock()
        mock_command_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = mock_command_instance
        case_created_handler = CaseCreatedHandler(mock_cqrs)
        assert case_created_handler.routing_keys == [
            "zsnl.v2.zsnl_domains_case_management.Case.CaseCreated"
        ]
        assert case_created_handler.cqrs == mock_cqrs

        mock_message = mock.MagicMock()
        mock_message.body = """
        {
            "changes": [
            {
                "key": "requestor",
                "old_value": null,
                "new_value": {
                    "type": "organization",
                    "id": "a4db1f5f-2c1b-411c-90bc-c2c57d5397fa"
                }
            }, {
                "key": "contact_channel",
                "old_value": null,
                "new_value": "email"
            }, {
                "key": "status",
                "old_value": null,
                "new_value": "new"
            }, {
                "key": "created_date",
                "old_value": null,
                "new_value": "2019-09-13 07:32:35.309085"
            }, {
                "key": "registration_date",
                "old_value": null,
                "new_value": "2019-09-13"
            }, {
                "key": "last_modified_date",
                "old_value": null,
                "new_value": "2019-09-13 07:32:35.309085"
            }, {
                "key": "milestone",
                "old_value": null,
                "new_value": 1
            }, {
                "key": "case_type_uuid",
                "old_value": null,
                "new_value": "d84e5728-48e9-4bd9-b111-1db0dd368e1b"
            }, {
                "key": "case_type_version_uuid",
                "old_value": null,
                "new_value": "54bcb5f7-6fd8-4b67-ba45-c5e594c9a8c3"
            }, {
                "key": "case_type_id",
                "old_value": null,
                "new_value": 3
            }, {
                "key": "case_type_node_id",
                "old_value": null,
                "new_value": 91
            }, {
                "key": "request_trigger",
                "old_value": null,
                "new_value": "extern"
            }, {
                "key": "group_id",
                "old_value": null,
                "new_value": "13ead360-66f6-4afd-a9b9-e9020375a71d"
            }, {
                "key": "route_role",
                "old_value": null,
                "new_value": "ed342b5c-8b87-4b01-a867-b6dfa7808d04"
            }, {
                "key": "department",
                "old_value": null,
                "new_value": "Development"
            }, {
                "key": "subject",
                "old_value": null,
                "new_value": ""
            }, {
                "key": "subject_extern",
                "old_value": null,
                "new_value": ""
            }, {
                "key": "target_completion_date",
                "old_value": null,
                "new_value": "2019-09-14"
            }, {
                "key": "confidentiality",
                "old_value": null,
                "new_value": "public"
            }
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": "d9579709-cd0d-499e-a01c-2d64a4b977af",
            "created_date": "2019-09-10T11:16:26.059411",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": "194cd271-e84e-43ed-b43c-cd18095e07c5",
            "entity_type": "Case",
            "event_name": "CaseCreated",
            "id": "e7cae74f-3da8-4944-8b16-c9ae6bffce44",
            "user_uuid": "1e2cbe4d-96a0-4578-8b29-5429f6ba9702"
        }
        """
        case_created_handler.handle(mock_message)
        mock_command_instance.enqueue_subcases.assert_called_once()
        mock_command_instance.enqueue_documents.assert_called_once()
        mock_command_instance.enqueue_subjects.assert_called_once()
        mock_command_instance.enqueue_emails.assert_called_once()
        mock_command_instance.transition_case.assert_called_once()

    def test_consumer(self):
        case_events_consumer = CaseEventsConsumer(
            queue="fake_queue_name",
            exchange="fake_exchange",
            cqrs=mock.MagicMock(),
            qos_prefetch_count=2,
            dead_letter_config={},
        )

        rk = case_events_consumer.routing_keys

        assert (
            set(rk)
            - {
                "zsnl.v2.zsnl_domains_case_management.Case.CaseCreated",
                "zsnl.v2.zsnl_domains_case_management.Case.DocumentsEnqueued",
                "zsnl.v2.zsnl_domains_case_management.Case.EmailsEnqueued",
                "zsnl.v2.zsnl_domains_case_management.Case.SubcasesEnqueued",
                "zsnl.v2.zsnl_domains_case_management.Case.SubjectsEnqueued",
                subject_relation_prefix + "SubjectRelationCreated",
                subject_relation_prefix + "SubjectRelationEmailEnqueued",
                case_prefix + "CaseAssigneeSet",
                case_prefix + "CaseAssigneeEmailEnqueued",
                "zsnl.v2.legacy.Case.CaseCustomFieldsUpdated",
                timeline_export_prefix + "TimelineExportRequested",
                case_legacy_prefix + "ObjectActionTriggered",
                case_prefix + "CustomFieldUpdated",
            }
            == set()
        )

    @mock.patch("zsnl_case_events_consumer.__main__.CQRS")
    @mock.patch("zsnl_case_events_consumer.__main__.InfrastructureFactory")
    @mock.patch("zsnl_case_events_consumer.__main__.AMQPClient")
    def test_main(self, mock_client, mock_infra, mock_cqrs):
        with mock.patch.object(__main__, "__name__", "__main__"):
            __main__.init()
            mock_client.assert_called()
            mock_client().register_consumers.assert_called_with(
                [CaseEventsConsumer]
            )
            mock_client().start.assert_called()

    def test_subject_relation_created_handler(self):
        mock_cqrs = mock.MagicMock()
        mock_cmd_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = mock_cmd_instance
        subject_relation_created_handler = SubjectRelationCreatedHandler(
            mock_cqrs
        )

        assert subject_relation_created_handler.routing_keys == [
            subject_relation_prefix + "SubjectRelationCreated"
        ]
        assert subject_relation_created_handler.cqrs == mock_cqrs

        event = mock.MagicMock()
        event.entity_id = uuid4()
        event.event_name = "SubjectRelationCreated"
        event.changes = [
            {
                "key": "send_confirmation_email",
                "old_value": None,
                "new_value": True,
            },
            {"key": "authorized", "old_value": None, "new_value": True},
        ]

        subject_relation_created_handler.handle(event)
        mock_cmd_instance.enqueue_subject_relation_email.assert_called_once()

    def test_subject_relation_email_enqueued_handler(self):
        mock_cqrs = mock.MagicMock()
        mock_command_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = mock_command_instance
        email_enqueued_handler = SubjectRelationEmailEnqueuedHandler(mock_cqrs)
        assert email_enqueued_handler.routing_keys == [
            subject_relation_prefix + "SubjectRelationEmailEnqueued"
        ]
        assert email_enqueued_handler.cqrs == mock_cqrs

        event = mock.MagicMock()
        event.entity_id = uuid4()
        event.event_name = "SubjectRelationEmailEnqueued"
        event.changes = []

        email_enqueued_handler.handle(event)
        mock_command_instance.send_subject_relation_email.assert_called_once()

    def test_case_attributes_changed_handler(self):
        mock_cqrs = mock.MagicMock()
        cmd_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = cmd_instance

        handler = CaseAttributesChangedHandler(mock_cqrs)

        assert handler.routing_keys == [
            "zsnl.v2.legacy.Case.CaseCustomFieldsUpdated"
        ]

        case_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = case_uuid
        event.event_name = "CaseCustomFieldsUpdated"
        event.changes = [
            {
                "key": "custom_fields",
                "old_value": [{"old": "x"}],
                "new_value": [{"new": "y"}],
            },
        ]

        handler.handle(event)
        cmd_instance.synchronize_relations_for_case.assert_called_once_with(
            case_uuid=str(case_uuid),
            custom_fields=event.format_changes()["custom_fields"],
        )

        # code-cov for CQRSException
        cmd_instance.synchronize_relations_for_case.side_effect = (
            CQRSException()
        )
        with self.assertLogs() as captured:
            handler.handle(event)
        assert (
            captured.records[0].getMessage()
            == "Error during relation sync; ignored: "
        )
        assert captured.records[0].levelno == logging.WARN

    def test_case_attribute_v2_changed_handler(self):
        mock_cqrs = mock.MagicMock()
        cmd_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = cmd_instance

        handler = CaseV2AttributeChangedHandler(mock_cqrs)

        assert handler.routing_keys == [
            "zsnl.v2.zsnl_domains_case_management.Case.CustomFieldUpdated"
        ]

        case_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = case_uuid
        event.event_name = "CustomFieldUpdated"
        event.format_changes.return_value = {"custom_fields": {"new": "y"}}

        handler.handle(event)
        cmd_instance.synchronize_relations_for_case.assert_called_once_with(
            case_uuid=str(case_uuid),
            custom_fields={"new": ['"y"']},
        )

        # code-cov for CQRSException
        cmd_instance.synchronize_relations_for_case.side_effect = (
            CQRSException()
        )
        with self.assertLogs() as captured:
            handler.handle(event)
        assert (
            captured.records[0].getMessage()
            == "Error during relation sync; ignored: "
        )
        assert captured.records[0].levelno == logging.WARN

    def test_case_attribute_v2_changed_handler_remove_relation_wrapper(self):
        mock_cqrs = mock.MagicMock()
        cmd_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = cmd_instance

        handler = CaseV2AttributeChangedHandler(mock_cqrs)

        assert handler.routing_keys == [
            "zsnl.v2.zsnl_domains_case_management.Case.CustomFieldUpdated"
        ]

        case_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = case_uuid
        event.event_name = "CustomFieldUpdated"
        event.format_changes.return_value = {
            "custom_fields": {
                "field_name": {
                    "type": "relationship",
                    "value": {"type": "relationship", "value": "a_uuid"},
                }
            }
        }

        handler.handle(event)
        cmd_instance.synchronize_relations_for_case.assert_called_once_with(
            case_uuid=str(case_uuid),
            custom_fields={
                "field_name": ['{"type": "relationship", "value": "a_uuid"}']
            },
        )

    def test_case_assignee_set_handler(self):
        mock_cqrs = mock.MagicMock()
        mock_cmd_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = mock_cmd_instance
        case_assignee_set_handler = CaseAssigneeSetHandler(mock_cqrs)

        assert case_assignee_set_handler.routing_keys == [
            case_prefix + "CaseAssigneeSet"
        ]
        assert case_assignee_set_handler.cqrs == mock_cqrs

        case_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = case_uuid
        event.event_name = "CaseAssigneeSet"
        event.changes = [
            {
                "key": "send_email_to_assignee",
                "old_value": None,
                "new_value": True,
            },
            {
                "key": "assignee",
                "old_value": None,
                "new_value": {"type": "subject", "entity_id": str(uuid4())},
            },
        ]

        case_assignee_set_handler.handle(event)
        mock_cmd_instance.enqueue_case_assignee_email.assert_called_once_with(
            case_uuid=case_uuid
        )

    def test_case_assignee_email_enqueued_handler(self):
        mock_cqrs = mock.MagicMock()
        mock_cmd_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = mock_cmd_instance
        assignee_email_enqueued_handler = CaseAssigneeEmailEnqueuedHandler(
            mock_cqrs
        )

        assert assignee_email_enqueued_handler.routing_keys == [
            case_prefix + "CaseAssigneeEmailEnqueued"
        ]
        assert assignee_email_enqueued_handler.cqrs == mock_cqrs

        case_uuid = uuid4()
        event = mock.MagicMock()
        event.entity_id = case_uuid
        event.event_name = "CaseAssigneeEmailEnqueued"
        queue_id = str(uuid4())
        event.changes = [
            {
                "key": "assignee_email_queue_id",
                "old_value": None,
                "new_value": queue_id,
            }
        ]

        assignee_email_enqueued_handler.handle(event)
        mock_cmd_instance.send_case_assignee_email.assert_called_once_with(
            case_uuid=case_uuid, queue_id=queue_id
        )

    def test_export_request_handler(self):
        mock_cqrs = mock.MagicMock()
        mock_cmd_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = mock_cmd_instance
        export_request_handler = TimelineExportRequestedHandler(mock_cqrs)

        assert export_request_handler.routing_keys == [
            timeline_export_prefix + "TimelineExportRequested"
        ]
        assert export_request_handler.cqrs == mock_cqrs

        event = mock.MagicMock()
        event.event_name = "TimelineExportRequested"
        period_start = "2021-02-03T12:53:00+01:00"
        period_end = "2021-02-04T12:53:00Z"
        entry_uuid = uuid4()
        entry_type = "person"
        event.changes = [
            {"key": "uuid", "old_value": None, "new_value": entry_uuid},
            {"key": "type", "old_value": None, "new_value": "person"},
            {
                "key": "period_start",
                "old_value": None,
                "new_value": period_start,
            },
            {"key": "period_end", "old_value": None, "new_value": period_end},
        ]

        export_request_handler.handle(event)
        mock_cmd_instance.run_export.assert_called_once_with(
            uuid=entry_uuid,
            type=entry_type,
            period_start=period_start,
            period_end=period_end,
        )

    def test_object_action_triggered_handler(self):
        mock_cqrs = mock.MagicMock()
        mock_cmd_instance = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = mock_cmd_instance
        object_action_triggered_handler = (
            ObjectActionTriggeredRequestedHandler(mock_cqrs)
        )

        assert object_action_triggered_handler.routing_keys == [
            case_legacy_prefix + "ObjectActionTriggered"
        ]

        assert object_action_triggered_handler.cqrs == mock_cqrs

        event = mock.MagicMock()
        event.event_name = "ObjectActionTriggered"
        entity_id = str(uuid4())
        event.entity_id = entity_id
        event.changes = [
            {"key": "action_type", "new_value": "edit", "old_value": None},
            {
                "key": "attribute_uuid",
                "new_value": "c2f00b8d-40ea-43d1-8b77-3949159d3f95",
                "old_value": None,
            },
            {
                "key": "attribute_magic_string",
                "new_value": "my_relation_attribute",
                "old_value": None,
            },
            {
                "key": "attribute_changes",
                "new_value": {
                    "object_attribute_number": "666",
                    "object_attribute_street": "street value",
                    "object_attribute_text": "text value",
                },
                "old_value": None,
            },
            {
                "key": "system_attribute_changes",
                "new_value": {},
                "old_value": None,
            },
        ]

        object_action_triggered_handler.handle(event)
        custom_object_fields = {
            "object_attribute_number": "666",
            "object_attribute_street": "street value",
            "object_attribute_text": "text value",
        }
        mock_cmd_instance.custom_object_action_trigger.assert_called_once_with(
            case_uuid=entity_id,
            action_type="edit",
            relation_field_magic_string="my_relation_attribute",
            custom_object_fields=custom_object_fields,
            status=True,
        )


class TestMain:
    @mock.patch("zsnl_case_events_consumer.__main__.CQRS")
    @mock.patch("zsnl_case_events_consumer.__main__.InfrastructureFactory")
    @mock.patch("zsnl_case_events_consumer.__main__.AMQPClient")
    def test_main(self, mock_client, mock_infra, mock_cqrs):
        with mock.patch.object(__main__, "__name__", "__main__"):
            __main__.init()
            mock_client.assert_called()
            mock_client().register_consumers.assert_called_with(
                [CaseEventsConsumer]
            )
            mock_client().start.assert_called()

    @mock.patch("zsnl_case_events_consumer.__main__.old_factory")
    def test_log_record_factory(self, old_factory):
        record = __main__.log_record_factory("a", test="b")

        old_factory.assert_called_once_with("a", test="b")
        assert record.zs_component == "zsnl_consumer_cm"  # type: ignore
        assert record == old_factory()
